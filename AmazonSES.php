<?php
	set_time_limit(120);
	require_once('mailer/PHPMailerAutoload.php');
	require_once('mailer/class.phpmailer.php');

	class AmazonSES
	{
		private $awsAccessKeyID;
		private $secretKey;
		private $smtp;
		private $username;
		private $password;
		private $secure;
		private $status;

		public function __construct($awsAccessKeyID, $secretKey, $endpoint, $smtp, $username, $password, $secure="ssl") {
			$this->awsAccessKeyID = $awsAccessKeyID;
			$this->secretKey = $secretKey;
			$this->endpoint = $endpoint;
			$this->smtp = $smtp;
			$this->username = $username;
			$this->password = $password;
			$this->secure = $secure;
		}

		public function verifyEmailIdentity($email) {
			
			$date = gmdate('D, d M Y H:i:s e');

			$parameters = array();			
			$parameters['AWSAccessKeyId'] = rawurlencode($this->awsAccessKeyID);
			$parameters['Action'] = rawurlencode('VerifyEmailIdentity');
			$parameters['SignatureVersion'] = 2;
			$parameters['SignatureMethod'] =rawurlencode('HmacSHA256');
			$parameters['Timestamp'] = rawurlencode(gmdate('Y-m-d\TH:i:s\Z'));
			$parameters['Version'] = rawurlencode('2010-12-01');
			$parameters['EmailAddress'] = rawurlencode($email);

			$parameters_string = '';
			foreach ($parameters as $key => $val) {
				$parameters_string .= $key . '=' . $val . '&';
			}
			$parameters_string = trim($parameters_string, "&");
			$signature = base64_encode(hash_hmac("sha256", $date, $this->secretKey, true));

			/*pr($parameters); 
			pr($signature);
			pr($parameters_string);
			die;*/

		 	$response = $this->connectAPI($signature, $date, $parameters_string); 

//pr($response); die;

			$doc = new DOMDocument();
			@$doc->loadXML($response);
			file_put_contents('response.xml', $response);

			if($this->status==200) {
				if($doc->getElementsByTagName('VerifyEmailIdentityResponse')->length > 0) {
					$error = 0;
					$message = 'Email address has been added, verification pending. Check inbox.';
				}
				else {
					$error = 1;
					$message = 'Response received but someting went wrong.';
				}
			}
			else {
				switch ($this->status) {
					case 0:
						$error = 1;
						$message = 'Error connecting API. Check system settings.';
						break;
					
					case 403:
						$message = 'API returned error.';
						if($doc->getElementsByTagName('Error')->length > 0) {
							$message =$doc->getElementsByTagName('Message')->item(0)->nodeValue;
						}
						$error = 1;
						break;

					default:
						$error = 1;
						$message = 'Something went wrong.';
						break;
				}
			}

			return json_encode(array('error'=>$error, 'message'=>$message));

		}

		public function sendMail($to, $from, $subject, $body, $name) {
			$mail = new PHPMailer(true);

			$mail->IsSMTP();

			try {
				$mail->Host       = $this->smtp;
				$mail->SMTPDebug  = 0;
				$mail->SMTPAuth   = true;
				$mail->SMTPSecure = $this->secure;

				$mail->Port       = 465;
				$mail->Username   = $this->username;
				$mail->Password   = $this->password;
				$mail->setFrom($from, $name);
				$mail->AddAddress($to);
				$mail->IsHTML(true);
				$mail->Subject = $subject;
				$mail->Body = $body;
				$mail->Send();
				$error = 0;
				$message = "Email was sent to " . $to;
			} 
			catch (phpmailerException $e) {
				$error = 1;
				$message = $e->errorMessage();
			}
			catch (Exception $e) {
				$error = 1;
				$message = $e->getMessage;
			}
			file_put_contents('report_' . date('Y_m_d') . '.txt', json_encode(array('error'=>$error, 'message'=>$message)), FILE_APPEND);

			return json_encode(array('error'=>$error, 'message'=>$message));
		}

		public function getAllVerifiedEmailAddresses() {
			$date = gmdate('D, d M Y H:i:s e');

			$parameters = array();			
			$parameters['AWSAccessKeyId'] = rawurlencode($this->awsAccessKeyID);
			$parameters['Action'] = rawurlencode('ListVerifiedEmailAddresses');
			$parameters['SignatureVersion'] = 2;
			$parameters['SignatureMethod'] =rawurlencode('HmacSHA256');
			$parameters['Timestamp'] = rawurlencode(gmdate('Y-m-d\TH:i:s\Z'));
			$parameters['Version'] = rawurlencode('2010-12-01');

			$parameters_string = '';
			foreach ($parameters as $key => $val) {
				$parameters_string .= $key . '=' . $val . '&';
			}
			$parameters_string = trim($parameters_string, "&");
			$signature = base64_encode(hash_hmac("sha256", $date, $this->secretKey, true));

			$response = $this->connectAPI($signature, $date, $parameters_string);

			$doc = new DOMDocument();
			@$doc->loadXML($response);

			if($this->status==200) {
				if($doc->getElementsByTagName('VerifiedEmailAddresses')->length > 0) {
					$error = 0;
					$length = $doc->getElementsByTagName('member')->length;
					$emailAddresses = array();
					for($i=0; $i<$length; $i++) {
						$emailAddresses[] = $doc->getElementsByTagName('member')->item($i)->nodeValue;
					}
					$message = $emailAddresses;
				}
				else {
					$error = 1;
					$message = 'Response received but someting went wrong.';
				}
			}
			else {
				switch ($this->status) {
					case 0:
						$error = 1;
						$message = 'Error connecting API. Check system settings.';
						break;
					
					case 403:
						$message = 'API returned error.';
						if($doc->getElementsByTagName('Error')->length > 0) {
							$message =$doc->getElementsByTagName('Message')->item(0)->nodeValue;
						}
						$error = 1;
						break;

					default:
						$error = 1;
						$message = 'Something went wrong.';
						break;
				}
			}

			return json_encode(array('error'=>$error, 'message'=>$message));
		}

		public function deleteEmailAddress($email) {
			$date = gmdate('D, d M Y H:i:s e');

			$parameters = array();			
			$parameters['AWSAccessKeyId'] = rawurlencode($this->awsAccessKeyID);
			$parameters['Action'] = rawurlencode('DeleteVerifiedEmailAddress');
			$parameters['SignatureVersion'] = 2;
			$parameters['SignatureMethod'] =rawurlencode('HmacSHA256');
			$parameters['Timestamp'] = rawurlencode(gmdate('Y-m-d\TH:i:s\Z'));
			$parameters['Version'] = rawurlencode('2010-12-01');
			$parameters['EmailAddress'] = rawurlencode($email);

			$parameters_string = '';
			foreach ($parameters as $key => $val) {
				$parameters_string .= $key . '=' . $val . '&';
			}
			$parameters_string = trim($parameters_string, "&");
			$signature = base64_encode(hash_hmac("sha256", $date, $this->secretKey, true));

			$response = $this->connectAPI($signature, $date, $parameters_string);

			$doc = new DOMDocument();
			@$doc->loadXML($response);
			file_put_contents('response.xml', $response);

			if($this->status==200) {
				if($doc->getElementsByTagName('DeleteVerifiedEmailAddressResponse')->length > 0) {
					$error = 0;
					$message = 'Email address removed.';
				}
				else {
					$error = 1;
					$message = 'Response received but someting went wrong.';
				}
			}
			else {
				switch ($this->status) {
					case 0:
						$error = 1;
						$message = 'Error connecting API. Check system settings.';
						break;
					
					case 403:
						$message = 'API returned error.';
						if($doc->getElementsByTagName('Error')->length > 0) {
							$message =$doc->getElementsByTagName('Message')->item(0)->nodeValue;
						}
						$error = 1;
						break;

					default:
						$error = 1;
						$message = 'Something went wrong.';
						break;
				}
			}

			return json_encode(array('error'=>$error, 'message'=>$message));
		}

		private function connectAPI($signature, $date, $params) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
														'X-Amzn-Authorization: AWS3-HTTPS AWSAccessKeyId=' . $this->awsAccessKeyID . ', Algorithm=HmacSHA256, Signature=' . $signature,
														'Content-Type: application/x-www-form-urlencoded',
														'x-amz-date: ' . $date
														));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_URL, $this->endpoint);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
			$result = curl_exec($ch);
			$this->status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			return $result;
		}
	}
?>