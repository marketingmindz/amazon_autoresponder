<?php
include('db.php');
include('header.php');

$query = mysql_query("select * from az_settings");
$rows = mysql_fetch_assoc($query);

$amazon_seller_id = $rows['amazon_seller_id'];
$marketplace_id = $rows['marketplace_id'];
$aws_access_key_id = $rows['aws_access_key_id'];
$secret_key = $rows['secret_key'];
$endpoint = $rows['endpoint'];
$mws_auth_token = $rows['mws_auth_token'];
$send_mail_option = $rows['send_mail_option'];
$watchdog_settings = $rows['watchdog_settings'];
$bad_review_email = $rows['bad_review_email'];
$account_username = $rows['account_username'];
$account_password = $rows['account_password'];
$email_own_replyto_db = $rows['email_own_replyto'];
$email_own_sender_db = $rows['email_own_sender'];
$email_smtp_host = $rows['email_smtp_host'];
$email_smtp_username = $rows['email_smtp_username'];
$email_smtp_password = $rows['email_smtp_password'];
$email_smtp_from = $rows['email_smtp_from'];
$email_smtp_port = $rows['email_smtp_port'];
$amazon_aws_email_key = $rows['amazon_aws_email_key'];
$amazon_secret_email_key = $rows['amazon_secret_email_key'];
$amazon_verified_email = $rows['amazon_verified_email'];
$database_host = $rows['database_host'];
$database_username = $rows['database_username'];
$database_password = $rows['database_password'];
$aws_associated_tag = $rows['aws_associated_tag'];
$aws_access_key = $rows['aws_access_key'];
$aws_secret_key = $rows['aws_secret_key'];
$special_review_notifcation = $rows['special_review_notifcation'];

if (isset($_REQUEST['api_submit'])) {
    $amazon_seller_id = $_POST['amazon_seller_id'];
    $marketplace_id = $_POST['marketplace_id'];
    $aws_access_key_id = $_POST['aws_access_key_id'];
    $secret_key = $_POST['secret_key'];
	$endpoint = $_POST['endpoint'];
    $mws_auth_token = $_POST['mws_auth_token'];
    $sql = "UPDATE az_settings SET amazon_seller_id='$amazon_seller_id',marketplace_id = '$marketplace_id',aws_access_key_id ='$aws_access_key_id',secret_key='$secret_key',mws_auth_token='$mws_auth_token'";
    $truncateoperation = "TRUNCATE TABLE az_products";
    mysql_query($truncateoperation);

    if (mysql_query($sql))
        echo "<span class='update-message'>Record updated successfully</span>";
    else
        echo "<span class='update-error-message'>Error updating record: " . mysql_error() . '</span>';
}

if (isset($_REQUEST['email_submit'])) {

    $send_mail_option = $_POST['send_mail_option'];
    if ($send_mail_option == 'smtp') {
        $email_smtp_host = $_POST['email_smtp_host'];
        $email_smtp_username = $_POST['email_smtp_username'];
        $email_smtp_password = $_POST['email_smtp_password'];
        $email_smtp_from = $_POST['email_smtp_from'];
        $email_smtp_port = $_POST['email_smtp_port'];
		$endpoint = $_POST['endpoint'];

		$check_from = mysql_query("select email_smtp_from from az_settings where email_smtp_from = '".$_POST['email_smtp_from']."' ");
		//$check_from_rows = mysql_fetch_assoc($check_from);
		$num_rows = mysql_num_rows($check_from);
	
		if($num_rows > 0){
			$sql = "UPDATE az_settings SET send_mail_option='$send_mail_option',email_smtp_host='$email_smtp_host',email_smtp_username= '$email_smtp_username',
			email_smtp_password = '$email_smtp_password',email_smtp_port='$email_smtp_port',email_smtp_from='$email_smtp_from',endpoint='$endpoint'";	
		}else{
			$sql = "UPDATE az_settings SET send_mail_option='$send_mail_option',email_smtp_host='$email_smtp_host',email_smtp_username= '$email_smtp_username',
			email_smtp_password = '$email_smtp_password',email_smtp_port='$email_smtp_port',email_smtp_from='$email_smtp_from',from_verify='no',
			endpoint='$endpoint'";	
		}
		
        
    } elseif ($send_mail_option == 'own') {
        $email_own_sender = $_POST['email_own_sender'];
        $email_own_replyto = $_POST['email_own_replyto'];
        $sql = "UPDATE az_settings SET send_mail_option='$send_mail_option',email_own_sender='$email_own_sender',email_own_replyto='$email_own_replyto'";
    }


    if (mysql_query($sql))
        echo "<span class='update-message'>Record updated successfully</span>";
    else
        echo "<span class='update-error-message'>Error updating record: " . mysql_error() . "</span>";
}

if (isset($_REQUEST['account_submit'])) {
    $account_username = $_POST['account_username'];
    $account_password = $_POST['account_password'];
    $confirm_account_password = $_POST['confirm_account_password'];
    $sql = "UPDATE az_settings SET account_username='$account_username',account_password='$account_password'";

    if (mysql_query($sql)) {
        $sql = "UPDATE az_users SET username='$account_username',password='$account_password'";
        mysql_query($sql);
        //header("location: logout.php");
        echo "<span class='update-message'>Record updated successfully</span>";
    } else
        echo "<span class='update-error-message'>Error updating record: " . mysql_error() . "</span>";
}

if (isset($_REQUEST['db_details'])) {
    $db_host = $_POST['db_host'];
    $db_username = $_POST['db_username'];
    $db_password = $_POST['db_password'];
    $dbsql = "UPDATE az_settings SET database_host='$db_host',database_username='$db_username',database_password='$db_password'";

    if (mysql_query($dbsql)) {
        echo "<span class='update-message'>Record updated successfully</span>";
    } else
        echo "<span class='update-error-message'>Error updating record: " . mysql_error() . "</span>";
}

if (isset($_REQUEST['aws_config'])) {
    $aws_associated_tag = $_POST['aws_associated_tag'];
    $aws_access_key = $_POST['aws_access_key'];
    $aws_secret_key = $_POST['aws_secret_key'];
    $awssql = "UPDATE az_settings SET aws_associated_tag='$aws_associated_tag',aws_access_key='$aws_access_key',aws_secret_key='$aws_secret_key'";

    if (mysql_query($awssql)) {
        echo "<span class='update-message'>Record updated successfully</span>";
    } else
        echo "<span class='update-error-message'>Error updating record: " . mysql_error() . "</span>";
}



if (isset($_REQUEST['watchdog_submit'])) {
    $bad_review_email = $_POST['bad_review_email'];
    $watchdog_settings = $_POST['watchdog_settings'];
    $special_review_notification = $_POST['special_review_notifcation'];
    $sql = "UPDATE az_settings SET watchdog_settings='$watchdog_settings',special_review_notifcation='$special_review_notification',bad_review_email='$bad_review_email'";

    if (mysql_query($sql)) {
        echo "<span class='update-message'>Record updated successfully</span>";
    } else
        echo "<span class='update-error-message'>Error updating record: " . mysql_error() . "</span>";
}
?>

<h2>Settings</h2>
<div class="container">
    <div class="half">
        <form action="" method="post" id="api_submit" name="api_submit" class="setting_form">
            <span class="text-orangr">Api Key Section</span>
            <div class="detail">Here you need to enter MWS account details.</div>
            <table>        
                <tr>
                    <td>Seller ID</td>
                    <td>
                        <input type="text" name="amazon_seller_id" id="amazon_seller_id" value="<?php echo $amazon_seller_id; ?>" required/>
                    </td>
                </tr>
                <tr>
                    <td>Marketplace ID</td>
                    <td>
                        <input type="text" name="marketplace_id" id="marketplace_id" value="<?php echo $marketplace_id; ?>" required/>
                    </td>
                </tr>
                <tr>
                    <td>AWS Access Key ID</td>
                    <td>
                        <input type="text" name="aws_access_key_id" id="aws_access_key_id" value="<?php echo $aws_access_key_id; ?>" required/>
                    </td>
                </tr>
                <tr>
                    <td>Secret Key</td>
                    <td>
                        <input type="text" name="secret_key" id="secret_key" value="<?php echo $secret_key; ?>" required/>
                    </td>
                </tr>
                
                <tr>
                    <td>MWS Auth Token</td>
                    <td>
                        <input type="text" name="mws_auth_token" id="mws_auth_token" value="<?php echo $mws_auth_token; ?>" required/>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" name="api_submit" id="api_submit" value="Save">
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <div class="half right">
        <form action="" method="post" id="amform" class="setting_form">
            <span class="text-orangr">Email Option </span>
            <div class="detail">Configure your SMTP Mail.</div>
            <table>            
                <tr>
                    <td style="width:50%">Select Your Email Service Preference </td>
                    <td>
                        <select name="send_mail_option" class="send_mail_option" required="required">
                            <option value="">---Please Select---</option>
                            <option value="smtp" id="smtp" <?php if ($send_mail_option == 'smtp') echo "selected"; ?>>Smtp</option>
                            <?php /*?><option value="own" <?php if ($send_mail_option == 'own') echo "selected"; ?> >Server Email</option><?php */?>
                        </select>
                    </td>
                </tr>                
                <tr class="smtp_options" <?php if ($send_mail_option != 'smtp') echo "style='display: none;'" ?>>
                    <td colspan="2">
                        <table class="full">
                            <tr>
                                <td style="width:50%">Smtp Host with protocol</td>
                                <td>
                                    <input type="text" name="email_smtp_host" id="email_smtp_host" value="<?php echo $email_smtp_host; ?>" placeholder="ex:smtp.gmail.com" required="required"/>
                               	</td>
                            </tr>
                            <tr>
                                <td>Smtp Username</td>
                                <td>
                                    <input type="text" name="email_smtp_username" id="email_smtp_username" value="<?php echo $email_smtp_username; ?>" required="required"/>
                               	</td>
                            </tr>
                            <tr>
                                <td>Smtp password</td>
                                <td>
                                    <input type="password" name="email_smtp_password" id="email_smtp_password" value="<?php echo $email_smtp_password; ?>" required="required"/>
                               	</td>
                            </tr>
                            <tr>
                                <td>Smtp port</td>
                                <td>
                                    <input type="text" name="email_smtp_port" id="email_smtp_port" value="<?php echo $email_smtp_port; ?>" required="required"/>
                               	</td>
                            </tr>
                            
                            <tr>
                                <td>Mail From</td>
                                <td>
                                    <input type="text" name="email_smtp_from" id="email_smtp_from" value="<?php echo $email_smtp_from; ?>" required="required"/>
                                </td>
                            </tr>
                            <tr>
                                <td>End point</td>
                                <td>
                                    <input type="text" name="endpoint" id="endpoint" value="<?php echo $endpoint; ?>" required/>
                                </td>
                			</tr>                
                        </table>
                    </td>
                </tr> 

                <!--                ---------------------------------- Server Email------------------------------------------>


                <?php /*?><tr class="own_options" <?php if ($send_mail_option != 'own') echo "style='display: none;'" ?>>
                    <td colspan="2">
                        <table class="full">
                            <tr>
                                <td style="width:50%">Sender Name</td>
                                <td>
                                    <input type="text" name="email_own_sender" id="email_own_sender" value="<?php echo $email_own_sender_db; ?>" required="required"/>
                               	</td>
                            </tr>
                            <tr>
                                <td>Reply to Address</td>
                                <td>
                                    <input type="text" name="email_own_replyto" id="email_own_replyto" value="<?php echo $email_own_replyto_db; ?>"/>
                               	</td>
                            </tr>


                        </table>
                    </td>
                </tr><?php */?>  

                <!--                ---------------------------------------- Server email Options End----------------------------->  
                <tr>
                    <td colspan="2"><input type="submit" name="email_submit" id="email_submit" value="Save"></td>
                </tr>
            </table>
        </form>
    </div>
</div>
<div class="cls"></div>
<div class="container">
    <div class="half">
        <form action="" method="post" id="amform" name="account_form" class="setting_form">
            <table>
                <span class="text-orangr">Account Login Details</span>
                <div class="detail">Change your account details from here</div>
                <tr>
                    <td>User Name</td>
                    <td><input type="text" name="account_username" id="account_username" value="<?php echo $account_username; ?>" required/></td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td><input type="password" name="account_password" id="account_password" required autocomplete="false"/></td>
                </tr>
                <tr>
                    <td>Confirm Password</td>
                    <td><input type="password" name="confirm_account_password" id="confirm_account_password" required autocomplete="false"/></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" name="account_submit" id="api_submit" value="Save"></td>
                </tr>
            </table>
        </form>
    </div>
    <div class="half right">
        <form action="" method="post" id="amform" class="setting_form">
            <span class="text-orangr">Watch Dog Configuration</span>
            <div class="detail">This section is required for watchdog</div>
            <table> 
                <tr>
                    <td>Special Bad Review Notification</td>
                    <td>
                        <select name="special_review_notifcation" class="special_review_notifcation" required="required">
                            <option value="">--Please Select---</option>
                            <option value="on" <?php if ($special_review_notifcation == 'on') echo "selected='selected'" ?>>On</option>
                            <option value="off" <?php if ($special_review_notifcation == 'off') echo "selected='selected'" ?>>Off</option>
                        </select>
                    </td>
                </tr>
                <tr class="bad_r_email" <?php if ($special_review_notifcation == 'off') echo "style='display: none;'" ?>>
                    <td>Bad Review Notification Email</td>
                    <td><input type="text" name="bad_review_email" id="bad_review_email" value="<?php echo $bad_review_email; ?>" required/></td>
                </tr>

                <tr>
                    <td>Watchdog Email</td>
                    <td><input type="text" name="watchdog_settings" id="watchdog_settings" value="<?php echo $watchdog_settings; ?>" required/></td>
                </tr>

                <tr>
                    <td></td>
                    <td><input type="submit" name="watchdog_submit" id="api_submit" value="Save"></td>
                </tr>
            </table>
        </form>
    </div>
</div>
<div class="cls"></div>
<div class="container">
    <div class="half">
        <form action="" method="post" id="databaseForm" name="databaseForm" class="setting_form">
            <span class="text-orangr">AWS Configuration</span>
            <div class="detail">You need to enter AWS account details for watchdog section and you need to have Advertising API activated under your AWS account</div>
            <table>            
                <tr>
                    <td>Account ID</td>
                    <td><input type="text" name="aws_associated_tag" id="aws_associated_tag" value="<?php echo $aws_associated_tag; ?>" required/></td>
                </tr>
                <tr>
                    <td>AWS Access key</td>
                    <td><input type="text" name="aws_access_key" id="aws_access_key" value="<?php echo $aws_access_key; ?>" required autocomplete="false"/></td>
                </tr>
                <tr>
                    <td>AWS secret key</td>
                    <td><input type="password" name="aws_secret_key" id="aws_secret_key" value="<?php echo $aws_secret_key; ?>" autocomplete="false"/></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" name="aws_config" id="aws_config" value="Save"></td>
                </tr>
            </table>
        </form>
    </div>
</div>
<?php include('footer.php') ?>