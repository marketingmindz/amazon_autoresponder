<?php
ob_start();
include('db.php');
include('header.php');


if (isset($_POST['template_submit'])) {
    $template_title = $_POST['template_title'];
    $template_subject = $_POST['template_subject'];
    $template_text = $_POST['template_text'];
    $sql = "INSERT INTO az_templates (template_name, template_subject, template_text)
VALUES ('$template_title', '$template_subject', '$template_text')";

    if (mysql_query($sql)) {
        echo "<span class='update-message'>Record updated successfully</span>";
        header("Location: template-section.php"); // Redirecting To Template Page
    } else
        echo "<span class='update-error-message'>Error updating record: " . mysql_error() . "</span>";
}
?>
<div class="dd">
<div class="ddd" style=" width:500px; margin:auto; ">
    <form action="" method="post" >
        <table style=" width:100%;">
        
        <tr>
        	<td colspan="2" > 
            	<h1 style="text-align: center !important;">Add New Email Template</h1>
            </td>
        </tr>
            <tr>
                <td>Title</td>
                <td><input type="text" name="template_title" id="template_title"></td>
            </tr> 
            <tr>
                <td>Subject</td>
                <td><input type="text" name="template_subject" id="template_subject"></td>
            </tr> 
            <tr>
                <td>Email text</td>
                <td><textarea name="template_text" class="ckeditor" rows="5"></textarea></td>
            </tr> 
            <tr><td></td><td><input type="submit" name="template_submit" value="Add"></td></tr>

        </table>


</form>

<div class=""  >
<div style=" width: 78%; border: 1px solid #ccc; padding: 15px;  margin:0 0 0 73px;;text-align: center;" class="ddd">
    <h2><u>Template Hints</u></h2>
    <br/>
    <span style="color: #000;">Use following variables in template text<br/></span>
    <span style="color: #000;">For Customer Name : [[customer_name]]<br/></span>
    <span style="color: #000;">For Order Number : [[order_number]]<br/></span>
    <span style="color: #000;">For Purchase Quantity : [[purchase_quantity]]<br/></span>
    <span style="color: #000;">For Ship Service Level : [[ship_service_level]]<br/></span>
    <span style="color: #000;">For Purchase Date : [[purchase_date]]<br/></span>
    <span style="color: #000;">For Payment Method : [[payment_method]]<br/></span>
    <span style="color: #000;">For Buyer Email : [[buyers_email]]<br/></span>
    <span style="color: #000;">For Buyer Address : [[buyer_address]]<br/></span>
    <span style="color: #000;">For Buyer Phone : [[buyer_phone]]<br/></span>
    <span style="color: #000;">For Order Id : [[order_id]]<br/></span>
    <span style="color: #000;"> For Purchased ASIN : [[purchased_asin]]<br/></span>
</div>

</div>
</div>
<?php include('footer.php') ?>

