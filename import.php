<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Upload page</title>
        <style type="text/css">
            body {
                background: #E3F4FC;
                font: normal 14px/30px Helvetica, Arial, sans-serif;
                color: #2b2b2b;
            }
            a {
                color:#898989;
                font-size:14px;
                font-weight:bold;
                text-decoration:none;
            }
            a:hover {
                color:#CC0033;
            }

            h1 {
                font: bold 14px Helvetica, Arial, sans-serif;
                color: #CC0033;
            }
            h2 {
                font: bold 14px Helvetica, Arial, sans-serif;
                color: #898989;
            }
            #container {
                background: #CCC;
                margin: 100px auto;
                width: 945px;
            }
            #form 			{padding: 20px 150px;}
            #form input     {margin-bottom: 20px;}
        </style>
    </head>
    <?php
    ob_start();
    include_once('db.php');
    include('header.php');
    ?>

    <?php  
    $tableName = "az_".$_REQUEST['page'];?>
        <div id="container">
            <div id="form">

                <?php
//$deleterecords = "TRUNCATE TABLE  az_templates"; //empty the table of its current records
//mysql_query($deleterecords);
//Upload File
                $import = '';
                if (isset($_POST['submit'])) {
                    if (is_uploaded_file($_FILES['filename']['tmp_name'])) {
                        echo "<h1>" . "File " . $_FILES['filename']['name'] . " uploaded successfully." . "</h1>";
                        //echo "<h2>Displaying contents:</h2>";
                        //readfile($_FILES['filename']['tmp_name']);
                    }


                    $result = mysql_query("SELECT * FROM $tableName WHERE 1");
                    $results = array();
                    while ($row = mysql_fetch_assoc($result)) {
                        $results[] = $row['id'];
                    }

                    $handle = fopen($_FILES['filename']['tmp_name'], "r");
                    $i = 0;
                    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                        $emCheck = array_search($data[0], $results);

                        if ($emCheck == '') {
                            if ($data[0] != 'id'){
                                if($_REQUEST['page']=='templates')
                                $import = "INSERT into az_templates(template_name,template_subject,template_text,created_date) values('$data[1]','$data[2]','$data[3]','$data[4]')";
                                elseif ($_REQUEST['page']=='products') 
                                   $import = "INSERT into az_products(product_asin,product_name,product_amount,inserted_date) values('$data[1]','$data[2]','$data[3]','$data[4]')"; 
                                elseif($_REQUEST['page']=='autoresponders')
                                $import = "INSERT into  az_autoresponders(template_name,product_asin,cron_time,created_date) values('$data[1]','$data[2]','$data[3]','$data[4]')";
                               elseif($_REQUEST['page']=='watchdog_section')
                                $import = "INSERT into  az_watchdog_section(asin) values('$data[1])"; 
                            
                            }

                            if ($import) {
                                mysql_query($import) or die(mysql_error());
                                $i++;
                            }
                        }
                    }

                    fclose($handle);

                    print "Import done";
                    if($_REQUEST['page']=='templates')
                    header("location: template-section.php");
                    
                    elseif ($_REQUEST['page']=='products') 
                         header("location: product-section.php");
                    //view upload form
                } else {
                        
                    print "Upload new csv by browsing to file and clicking on Upload<br />\n";

                    print "<form enctype='multipart/form-data' action='' method='post'>";

                    print "File name to import:<br />\n";

                    print "<input size='50' type='file' name='filename'><br />\n";

                    print "<input type='submit' name='submit' value='Upload'></form>";
                }
                ?>

            </div>
        </div>
  <?php include('footer.php'); ?>