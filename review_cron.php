<?php
require_once 'db.php';
require_once 'header.php';
require_once('AmazonSES.php');

$truncateoperation_details = "TRUNCATE TABLE az_reviews_details";
mysql_query($truncateoperation_details);
//error_reporting(0);
$query = mysql_query("select * from az_settings");
$rows = mysql_fetch_assoc($query);
$watchdogFromEmail = $rows['watchdog_settings'];
$bad_review_email =$rows['bad_review_email'];
$AssociateTag = $rows['aws_associated_tag'];
$AWSAccessKeyId = $rows['aws_access_key'];
$SecretAccessKey = $rows['aws_secret_key'];
$email_smtp_host = $rows['email_smtp_host'];
$email_smtp_username = $rows['email_smtp_username'];
$email_smtp_password = $rows['email_smtp_password'];
$endpoint = $rows['endpoint'];
$email_smtp_from = $rows['email_smtp_from'];
$special_notification = $rows['special_review_notifcation'];
echo $newsql = "select * from az_watchdog_section";
$newquery = mysql_query($newsql);
while ($getwatchdogasin = mysql_fetch_array($newquery)):
    echo $getwatchdogasin['asin'];

    $ItemId = $getwatchdogasin['asin'];  // ASIN
    $Timestamp = gmdate("Y-m-d\TH:i:s\Z");
    $Timestamp = str_replace(":", "%3A", $Timestamp);
    $ResponseGroup = "Reviews";
    $ResponseGroup = str_replace(",", "%2C", $ResponseGroup);

    $String = "AWSAccessKeyId=$AWSAccessKeyId&AssociateTag=$AssociateTag&ItemId=$ItemId&Operation=ItemLookup&ResponseGroup=$ResponseGroup&Service=AWSECommerceService&Timestamp=$Timestamp&Version=2011-08-01";

    $String = str_replace("\n", "", $String);

    $Prepend = "GET\nwebservices.amazon.com\n/onca/xml\n";
    $PrependString = $Prepend . $String;

    $Signature = base64_encode(hash_hmac("sha256", $PrependString, $SecretAccessKey, True));
    $Signature = str_replace("+", "%2B", $Signature);
    $Signature = str_replace("=", "%3D", $Signature);

    $BaseUrl = "http://webservices.amazon.com/onca/xml?";
    $SignedRequest = $BaseUrl . $String . "&Signature=" . $Signature;

    $XML = simplexml_load_file($SignedRequest);
    //echo "<pre>";
    $startingUrl = $XML->Items->Item->CustomerReviews->IFrameURL;

    $html = file_get_contents($startingUrl); //get the html returned from the following url
    $pokemon_doc = new DOMDocument();

    libxml_use_internal_errors(TRUE); //disable libxml errors
    $doc = new DOMDocument();
    $doc->loadHTML($html);

    $xpath = new DOMXpath($doc);
    $articles = $xpath->query('//span[@class="small"]');
	// all links in .blogArticle
    $links = array();
    foreach ($articles as $container) {
        $arr = $container->getElementsByTagName("a");
        foreach ($arr as $item) {
            $href = $item->getAttribute("href");
            $text = trim(preg_replace("/[\r\n]+/", " ", $item->nodeValue));
            $links[] = array(
                'href' => $href,
                'text' => $text
            );
        }
    }

    $amazonLink = $links[0]['href'];
    $totalReviewsText = $links[0]['text'];
    $explodeReviewText = explode(' ', $totalReviewsText);
    $totalReview = $explodeReviewText[2];
   echo  $amazonLink;
    $oldReview = '';
    $checkAsinStatus = mysql_query("select * from az_reviews_status where asin='$ItemId'");

    $getdata = mysql_fetch_array($checkAsinStatus);
   //print_r($getdata); die;
    $oldReview = $getdata['last_review_count'];
    if ($getdata) {
        $sqlupdate = "UPDATE az_reviews_status SET last_review_count ='$totalReview' where asin='$ItemId'";
        mysql_query($sqlupdate);
    } else {
        $reviewCountInsert = "INSERT INTO az_reviews_status (asin,last_review_count,email_status)
		VALUES ('$ItemId', '$totalReview', 'n')";
        mysql_query($reviewCountInsert);
    }

    $newReviews = $totalReview - $oldReview;

    /* ------------------------------ Second phase scraping-------------------------------------------- */
	
	 echo "<br>".$newReviews."<br>"; 
	 echo $totalReview."<br>"; 
	 echo $oldReview."<br>";
	
	 if ($oldReview != 0 && $oldReview != '' && $newReviews != 0) {
        $htmls = file_get_contents($amazonLink);
        $pokemon_docs = new DOMDocument();
        libxml_use_internal_errors(TRUE); //disable libxml errors
        $docs = new DOMDocument();
        $docs->loadHTML($htmls);
       
	  	$xpath = new DOMXpath($docs);
    	$reviews = $xpath->query('//div[@class="a-section review"]');	
	   	
		echo "<pre>";
		pr($reviews);
		
		$review_details;		
		$j=0;
		foreach ($reviews as $o_review) {
			
			if($j < $newReviews){
				
				$rating_section = $o_review->getElementsByTagName('div');
				foreach ($rating_section as $o_rating_section) {
					$childNodeList = $o_rating_section->getElementsByTagName('a');	
						# get  Review Title
						for ($i = 0; $i < $childNodeList->length; $i++) {
							$temp = $childNodeList->item($i);
							if(stripos($temp->getAttribute('class'),'review-title') !== false){
								//echo "<br>";
								//echo $temp->textContent;
								$review_details['reviewTitle'] = str_replace("'",'',$temp->textContent);
							} 					
						}
				
						# get Review Rating
						foreach ($childNodeList as $childNodeList) {
							$ratingchildNodeList = $childNodeList->getElementsByTagName('span');
							for ($i = 0; $i < $ratingchildNodeList->length; $i++) {
								$temp = $ratingchildNodeList->item($i);
								if(stripos($temp->getAttribute('class'),'a-icon-alt') !== false) {
									//echo "<br>";
									//echo $temp->textContent;
									$review_details['reviewRating'] = $temp->textContent;
								}
							}
						}
				
					# get Review Date
					$datechildNodeList = $o_rating_section->getElementsByTagName('span');	
					for ($i = 0; $i < $datechildNodeList->length; $i++) {
						$temp = $datechildNodeList->item($i);
						if(stripos($temp->getAttribute('class'),'review-date') !== false){
							//echo "<br>";
							//echo $temp->textContent;
							$review_details['reviewDate'] = $temp->textContent;
						} 
					}
					
					# get Review Text
					$textchildNodeList = $o_rating_section->getElementsByTagName('span');	
					for ($i = 0; $i < $textchildNodeList->length; $i++) {
						$temp = $textchildNodeList->item($i);
						if(stripos($temp->getAttribute('class'),'review-text') !== false){
							//echo "<br>";
							//echo $temp->textContent;
							$review_details['reviewText'] = str_replace("'","",$temp->textContent);
						} 
					}
			}
			pr($review_details);	
			echo $sql = "INSERT INTO az_reviews_details (asin,review_date, review_title,review_score,review_text) VALUES ('$ItemId','".$review_details['reviewDate']."', '".$review_details['reviewTitle']."','".$review_details['reviewRating']."','".$review_details['reviewText']."')";
			mysql_query($sql);
			$j=$j+1;
			
			
			
		}
		
	}
	echo "<br>".$j;		
}
	
endwhile;


// Check from is verified  or not. If yes then set Amazon SES Mail
$check_from = mysql_query("select from_verify from az_settings where email_smtp_from = '$email_smtp_from' ");
$check_from_rows = mysql_fetch_assoc($check_from);
$ob = new AmazonSES($AWSAccessKeyId, $SecretAccessKey, $endpoint , $email_smtp_host, $email_smtp_username,  $email_smtp_password);	
if($check_from_rows['from_verify'] == 'no'){
	$veriffyresult = $ob->verifyEmailIdentity('kapil@marketingmindz.in');
	$sql = "UPDATE az_settings SET from_verify='yes'"; 
	mysql_query($sql);
}else{
	$send_ses_mail = 'yes';
}


$queryforwd = mysql_query("select * from az_reviews_details");
while ($newrows = mysql_fetch_assoc($queryforwd)):
    $newrowsreview_asin = $newrows['asin'];
    $subject = 'New Review of ' . $newrowsreview_asin;
    $newrowsreview_title = $newrows['review_title'];
    $newrowsreview_score = $newrows['review_score'];
    $newrowsreview_date = $newrows['review_date'];
    $newrowsreview_text = $newrows['review_text'];
    $messagebody = "Review Title : $newrowsreview_title<br/>Review Date:$newrowsreview_date<br/>Review Score:$newrowsreview_score<br/>Review Text:$newrowsreview_text";
	//$to = $watchdogFromEmail;
	//$to = $email_smtp_from;
	$to = 'kapil.dev190@gmail.com';
	$email_smtp_from = "kapil@marketingmindz.com";
	if($send_ses_mail == 'yes'){
		# Mail via Amazon SES using mailer
		echo  $result = $ob->sendMail($to, $email_smtp_from, $subject, $messagebody, $email_smtp_from);
	}else{
		# Mail via simple phpmailer mailer
		//Send_Mail($watchdogFromEmail, $subject, $messagebody);
		Send_Mail($to, $subject, $messagebody);	
	}

endwhile;

if ($special_notification == 'on') {
    $specialReview = mysql_query("select * from az_reviews_details WHERE review_score < 2");

	while ($specialReviewfetch = mysql_fetch_assoc($specialReview)):
        
        $newrowsreview_asin = $specialReviewfetch['asin'];
        $subject = 'One Bad Review of ' . $newrowsreview_asin;
        $newrowsreview_title = $specialReviewfetch['review_title'];
        $newrowsreview_score = $specialReviewfetch['review_score'];
        $newrowsreview_date = $specialReviewfetch['review_date'];
        $newrowsreview_text = $specialReviewfetch['review_text'];
        $messagebody = "Review Title : $newrowsreview_title<br/>Review Date:$newrowsreview_date<br/>Review Score:$newrowsreview_score<br/>Review Text:$newrowsreview_text";
        $to = $bad_review_email;
		if($send_ses_mail == 'yes'){
			# Mail via Amazon SES using mailer
			//$result = $ob->sendMail($to, $email_smtp_from, $subject, $body, $email_smtp_from);
			$result = $ob->sendMail($to, $email_smtp_from, $subject, $body, $email_smtp_from);
			
		}else{
			# Mail via simple phpmailer mailer
			//Send_Mail($bad_review_email, $subject, $messagebody);
			Send_Mail($to, $subject, $messagebody);	
		}
	   
    endwhile;
	
// Empty az_reviews_details  here	
	
}
?>