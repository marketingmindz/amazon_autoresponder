<?php
include('db.php');
include('header.php');
?>

<div class="top-tabed">
    <div class="add_new link_1" style="float: right;"> <a href="import.php?page=autoresponders" class="link_1">Import</a> | <a href="export.php?page=autoresponders" class="link_1">Export All</a> </div>

</div>
<?php
date_default_timezone_set("Asia/Calcutta");

if (isset($_POST['addAr'])) {
    $template_id = $_POST['template_id'];
    $product_asin = $_POST['product_asin'];
    $cron_time = $_POST['cron_time'];
    $addTime = strtotime("+" . $cron_time . "hours");
    //$finalDate = date('y-m-d H', $addTime);
	
	#get template name 
	$query = mysql_query("select template_name from az_templates where id = '".$template_id."' ");
	$rows = mysql_fetch_row($query);
	$template_name = $rows[0];
		
    $sql = "INSERT INTO az_autoresponders (template_id,template_name, product_asin, cron_time,running_time)
VALUES ($template_id,'$template_name', '$product_asin', '$cron_time','$addTime')";

    if (mysql_query($sql)) {
        echo "<span style='color:green;font-weight:bold;text-align:center;'>Record updated successfully</span>";
    } else
        echo "Error updating record: " . mysql_error();
}
?>
<?php
echo ' <div class="autoresponder__left_main"><h1 class="text_new_mm">Active Autoresponders</h1>';

$page = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
if ($page <= 0)
    $page = 1;

$per_page = 5; // Set how many records do you want to display per page.

$startpoint = ($page * $per_page) - $per_page;

$statement = "`az_autoresponders` ORDER BY `id` ASC"; // Change `records` according to your table name.

$results = mysql_query("SELECT * FROM {$statement} LIMIT {$startpoint} , {$per_page}");

if (mysql_num_rows($results) != 0) {

    $query = mysql_query("select * from az_autoresponders");
    echo '<div><table border=0 cellspacing=0 class=" email-templates">';
    echo '<tr><th>Template Name</th><th>Product ASIN</th><th>Time</th><th colspan="2">Action</th></tr>';
    $count = 1;
    while ($rows = mysql_fetch_array($results)):
        if ($count % 2 == 0) {
        $class = " class='orange'";
    } else {
        $class = " class='grey'";
    }
        if ($rows["cron_time"] > 24)
            $showcronTime = $rows['cron_time'] / 24 . " Days";
        else
            $showcronTime = $rows['cron_time'] . " Hour";

        echo '<tr '. $class .'><td>' . $rows['template_name'] . '</td><td>' . $rows['product_asin'] . '</td><td>' . $showcronTime . ' </td><td class="icons"><button class="edit-icon" id="edittemplate" editid="' . $rows['id'] . '" onclick=' . 'location.href=' . '"edit-autoresponder.php?autoresponder_id=' . $rows['id'] . '' . '"></button>  &nbsp;  <button class="deleteautoresponder delete-icon" deleteautoresponderid="' . $rows['id'] . '"></button></td>

     </tr>';
        $count++;
    endwhile;
   echo '</table></div>';
    echo pagination($statement, $per_page, $page, $url = '?');
    echo '</div>';
} else {
    echo "You haven’t set up an Autoresponder yet.";
}

// displaying paginaiton.
?>
<div class="new-autoresponder__right_main"><h1 class="text_new_mm">Add New Autoresponder</h1>
    <form action="" method="post" name="autoresponderForm">
        <select id="select" name="template_id" style="width:90%; margin-bottom:12px;" required="required">
            <option value="">----Select Template----</option>
            <?php
            $sql = "Select * from az_templates";
            $raws = mysql_query($sql);
            while ($results = mysql_fetch_array($raws))
                echo '<option value=' . $results["id"] . '>' . $results["template_name"] . '</option>'
                ?>    
        </select>
        <select id="select" name="product_asin" style="width:90%; margin-bottom:12px;" required="required">
            <option value="">----Select ASIN----</option>
            <?php
            $AsinSql = "Select * from az_products";
            $raws = mysql_query($AsinSql);
            while ($results = mysql_fetch_array($raws))
                echo '<option value=' . $results["product_asin"] . '>' . $results["product_asin"] . '</option>'
                ?>    
        </select>

        <select id="select" name="cron_time" style="width:90%; margin-bottom:12px;" required="required">
            <option value="">----Select Time----</option>
            <option value="1">1 Hour</option>
            <option value="12">12 Hours</option>
            <option value="24">24 Hours</option>
            <option value="48">2 Days</option>
            <option value="72">3 Days</option>
            <option value="120">5 Days</option>
            <option value="144">6 Days</option>
            <option value="168">7 Days</option>
            <option value="336">14 Days</option>
            <option value="720">1 Month</option>
        </select>
        <input type="submit" name="addAr" value="Add Autoresponder"/>
    </form></div>
<?php include('footer.php') ?>