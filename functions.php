<script>
    jQuery(document).ready(function () {

        jQuery("form[name=api_submit]").bind('submit', function () {
            cnfrm = confirm("Are you sure to change the api key! your all data will be removed");
            if (cnfrm)
                return true;
            else
                return false;
        });
        jQuery('#confirm_account_password').focusout(function () {
            pasword = jQuery('#account_password').val();
            confirm_pasword = jQuery('#confirm_account_password').val();
            if (pasword != confirm_pasword) {
                alert("Confirm password is not same");
                jQuery('#confirm_account_password').val('');
//                jQuery('input[type="submit"]').attr('disabled', 'disabled');
                if (event.keyCode == 10 || event.keyCode == 13)
                    event.preventDefault();
            }
        });
        jQuery("form[name=account_form]").bind('submit', function () {
            pasword = jQuery('#account_password').val();
            confirm_pasword = jQuery('#confirm_account_password').val();
            if (pasword != confirm_pasword) {
                alert("Confirm password is not same");
                return false;
            }
        });

        jQuery('.deletetemplate').click(function () {
            var cnfrm;
            cnfrm = confirm("Are you sure to delete this template");
            if (cnfrm) {
                jQuery.ajax({
                    type: "POST",
                    url: "delete-template.php",
                    data: {deleteid: jQuery(this).attr('deleteid'), pagename: 'template-section'}
                })
                        .done(function (msg) {
                            alert(msg);
                            location.reload();
                        });
            }
        });

        jQuery('.deleteproduct').click(function () {
            var procnfrm;
            procnfrm = confirm("Are you sure to delete this product");
            if (procnfrm) {
                jQuery.ajax({
                    type: "POST",
                    url: "delete-template.php",
                    data: {deleteid: jQuery(this).attr('productdeleteid'), pagename: 'product-section'}
                })
                        .done(function (msg) {
                            alert(msg);
                            location.reload();
                        });
            }
        });
        
        jQuery('.deleteautoresponder').click(function () {
            var cnfrm;
            cnfrm = confirm("Are you sure to delete this Auto-Responder?");
            if (cnfrm) {
                jQuery.ajax({
                    type: "POST",
                    url: "delete-template.php",
                    data: {deleteautoresponderid: jQuery(this).attr('deleteautoresponderid'), pagename: 'autoresponder-section'}
                })
                        .done(function (msg) {
                            alert(msg);
                            location.reload();
                        });
            }
        });
        
        jQuery('.reset_all_states').click(function () {
            var cnfrm;
            cnfrm = confirm("Are you sure to reset all states");
            if (cnfrm) {
                jQuery.ajax({
                    type: "POST",
                    url: "delete-template.php",
                    data: {pagename: 'states-section'}
                })
                        .done(function (msg) {
                            alert(msg);
                            location.reload();
                        });
            }
        });
        
        jQuery('.deleteWatchdogProduct').click(function () {
            var cnfrm;
            cnfrm = confirm("Are you sure to delete this product from watchdog section");
            if (cnfrm) {
                jQuery.ajax({
                    type: "POST",
                    url: "delete-template.php",
                    data: {deleteid: jQuery(this).attr('watchdogdeleteid'),pagename: 'watchdog-section'}
                })
                        .done(function (msg) {
                            alert(msg);
                            location.reload();
                        });
            }
        });
        

        jQuery('.send_mail_option').change(function () {
            selectedval = jQuery('.send_mail_option').val();
            if (selectedval == 'smtp')
            {
                jQuery('.smtp_options').show();
                jQuery('.amazon_ses_options').hide();
            }
            else if (selectedval == 'own') {
                jQuery('.smtp_options').hide();
                jQuery('.own_options').show();
               
            }
           
        });
           jQuery('.special_review_notifcation').change(function () {
          var res = jQuery(this).val();
          if(res=='on')
              jQuery('.bad_r_email').show();
          if(res=='off')
              jQuery('.bad_r_email').hide();
        });


    });
</script>

<?php

function pr($args) {
    echo "<pre>";
    print_r($args);
    echo "</pre>";
    return;
}

function pagination($query, $per_page , $page = 1, $url = '?') {
    global $conDB;
    $query = "SELECT COUNT(*) as `num` FROM {$query}";
    $row = mysql_fetch_array(mysql_query($query));
    $total = $row['num'];
    $adjacents = "2";

    $prevlabel = "&lsaquo; Prev";
    $nextlabel = "Next &rsaquo;";
    $lastlabel = "Last &rsaquo;&rsaquo;";

    $page = ($page == 0 ? 1 : $page);
    $start = ($page - 1) * $per_page;

    $prev = $page - 1;
    $next = $page + 1;

    $lastpage = ceil($total / $per_page);

    $lpm1 = $lastpage - 1; // //last page minus 1

    $pagination = "";
    if ($lastpage > 1) {
        $pagination .= "<ul class='pagination'>";
        $pagination .= "<li class='page_info'>Page {$page} of {$lastpage}</li>";

        if ($page > 1)
            $pagination.= "<li><a href='{$url}page={$prev}'>{$prevlabel}</a></li>";

        if ($lastpage < 7 + ($adjacents * 2)) {
            for ($counter = 1; $counter <= $lastpage; $counter++) {
                if ($counter == $page)
                    $pagination.= "<li><a class='current'>{$counter}</a></li>";
                else
                    $pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";
            }
        } elseif ($lastpage > 5 + ($adjacents * 2)) {

            if ($page < 1 + ($adjacents * 2)) {

                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                    if ($counter == $page)
                        $pagination.= "<li><a class='current'>{$counter}</a></li>";
                    else
                        $pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";
                }
                $pagination.= "<li class='dot'>...</li>";
                $pagination.= "<li><a href='{$url}page={$lpm1}'>{$lpm1}</a></li>";
                $pagination.= "<li><a href='{$url}page={$lastpage}'>{$lastpage}</a></li>";
            } elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {

                $pagination.= "<li><a href='{$url}page=1'>1</a></li>";
                $pagination.= "<li><a href='{$url}page=2'>2</a></li>";
                $pagination.= "<li class='dot'>...</li>";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                    if ($counter == $page)
                        $pagination.= "<li><a class='current'>{$counter}</a></li>";
                    else
                        $pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";
                }
                $pagination.= "<li class='dot'>..</li>";
                $pagination.= "<li><a href='{$url}page={$lpm1}'>{$lpm1}</a></li>";
                $pagination.= "<li><a href='{$url}page={$lastpage}'>{$lastpage}</a></li>";
            } else {

                $pagination.= "<li><a href='{$url}page=1'>1</a></li>";
                $pagination.= "<li><a href='{$url}page=2'>2</a></li>";
                $pagination.= "<li class='dot'>..</li>";
                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                    if ($counter == $page)
                        $pagination.= "<li><a class='current'>{$counter}</a></li>";
                    else
                        $pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";
                }
            }
        }

        if ($page < $counter - 1) {
            $pagination.= "<li><a href='{$url}page={$next}'>{$nextlabel}</a></li>";
            $pagination.= "<li><a href='{$url}page=$lastpage'>{$lastlabel}</a></li>";
        }

        $pagination.= "</ul>";
    }

    return $pagination;
}

function pagination2($query, $per_page , $page , $url = '?') {
	global $conDB;
	global $limit;
	$total = mysql_num_rows(mysql_query($query));
	$subsql = " LIMIT ".$page*$limit." , ".$limit." ";
   	$query .= $subsql;
    $row = mysql_fetch_array(mysql_query($query));
   
    $adjacents = "2";

    $prevlabel = "&lsaquo; Prev";
    $nextlabel = "Next &rsaquo;";
    $lastlabel = "Last &rsaquo;&rsaquo;";

    $page = ($page == 0 ? 1 : $page);
    $start = ($page - 1) * $per_page;

    $prev = $page - 1;
    $next = $page + 1;

    $lastpage = ceil($total / $per_page);

    $lpm1 = $lastpage - 1; // //last page minus 1

    $pagination = "";
    if ($lastpage > 1) {
        $pagination .= "<ul class='pagination'>";
        $pagination .= "<li class='page_info'>Page {$page} of {$lastpage}</li>";

        if ($page > 1)
            $pagination.= "<li><a href='{$url}page={$prev}'>{$prevlabel}</a></li>";

        if ($lastpage < 7 + ($adjacents * 2)) {
            for ($counter = 1; $counter <= $lastpage; $counter++) {
                if ($counter == $page)
                    $pagination.= "<li><a class='current'>{$counter}</a></li>";
                else
                    $pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";
            }
        } elseif ($lastpage > 5 + ($adjacents * 2)) {

            if ($page < 1 + ($adjacents * 2)) {

                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                    if ($counter == $page)
                        $pagination.= "<li><a class='current'>{$counter}</a></li>";
                    else
                        $pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";
                }
                $pagination.= "<li class='dot'>...</li>";
                $pagination.= "<li><a href='{$url}page={$lpm1}'>{$lpm1}</a></li>";
                $pagination.= "<li><a href='{$url}page={$lastpage}'>{$lastpage}</a></li>";
            } elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {

                $pagination.= "<li><a href='{$url}page=1'>1</a></li>";
                $pagination.= "<li><a href='{$url}page=2'>2</a></li>";
                $pagination.= "<li class='dot'>...</li>";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                    if ($counter == $page)
                        $pagination.= "<li><a class='current'>{$counter}</a></li>";
                    else
                        $pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";
                }
                $pagination.= "<li class='dot'>..</li>";
                $pagination.= "<li><a href='{$url}page={$lpm1}'>{$lpm1}</a></li>";
                $pagination.= "<li><a href='{$url}page={$lastpage}'>{$lastpage}</a></li>";
            } else {

                $pagination.= "<li><a href='{$url}page=1'>1</a></li>";
                $pagination.= "<li><a href='{$url}page=2'>2</a></li>";
                $pagination.= "<li class='dot'>..</li>";
                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                    if ($counter == $page)
                        $pagination.= "<li><a class='current'>{$counter}</a></li>";
                    else
                        $pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";
                }
            }
        }

        if ($page < $counter - 1) {
            $pagination.= "<li><a href='{$url}page={$next}'>{$nextlabel}</a></li>";
            $pagination.= "<li><a href='{$url}page=$lastpage'>{$lastlabel}</a></li>";
        }

        $pagination.= "</ul>";
    }

    return $pagination;
}


include_once 'PHPmailer/Send_Mail.php';
//$to = "ravi_khandelwal@marketingmindz.in";
//$subject = "Test Mail Subject";
//$body = "Hi<br/>Test Mail<br/>Amazon SES"; // HTML  tags
//Send_Mail($to, $subject, $body);
////if(Send_Mail($to,$subject,$body))
////        echo "Sent";
////    else 
////        echo "Fail";
?>


