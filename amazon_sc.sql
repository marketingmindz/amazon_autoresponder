-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 27, 2015 at 07:20 AM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `amazon_sc`
--

-- --------------------------------------------------------

--
-- Table structure for table `az_autoresponders`
--

CREATE TABLE IF NOT EXISTS `az_autoresponders` (
`id` int(11) NOT NULL,
  `template_name` varchar(100) NOT NULL,
  `product_asin` varchar(100) NOT NULL,
  `cron_time` int(10) NOT NULL,
  `running_time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `az_customer_details`
--

CREATE TABLE IF NOT EXISTS `az_customer_details` (
`id` int(11) NOT NULL,
  `product_asin` varchar(20) NOT NULL,
  `purchase_quantity` varchar(11) NOT NULL,
  `ship_service_level` varchar(100) NOT NULL,
  `purchase_date` varchar(100) NOT NULL,
  `payment_method` varchar(100) NOT NULL,
  `buyers_email` varchar(50) NOT NULL,
  `AmazonOrderId` varchar(100) NOT NULL,
  `BuyerName` varchar(100) NOT NULL,
  `BuyerAddress` text NOT NULL,
  `BuyerPhone` varchar(75) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `az_email_statistics`
--

CREATE TABLE IF NOT EXISTS `az_email_statistics` (
`id` int(11) NOT NULL,
  `sender_name` varchar(100) NOT NULL,
  `sender_email` varchar(150) NOT NULL,
  `template_name` varchar(100) NOT NULL,
  `purchased_asin` varchar(50) NOT NULL,
  `send_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `az_products`
--

CREATE TABLE IF NOT EXISTS `az_products` (
`id` int(11) NOT NULL,
  `product_asin` varchar(10) NOT NULL,
  `product_name` text NOT NULL,
  `product_amount` float NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `az_reviews_details`
--

CREATE TABLE IF NOT EXISTS `az_reviews_details` (
`id` int(11) NOT NULL,
  `asin` varchar(50) NOT NULL,
  `review_date` varchar(100) NOT NULL,
  `review_title` varchar(255) NOT NULL,
  `review_score` int(10) NOT NULL,
  `review_text` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `az_reviews_status`
--

CREATE TABLE IF NOT EXISTS `az_reviews_status` (
`id` int(10) NOT NULL,
  `asin` varchar(50) NOT NULL,
  `last_review_count` int(10) NOT NULL,
  `email_status` enum('y','n') NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `az_settings`
--

CREATE TABLE IF NOT EXISTS `az_settings` (
`id` int(5) NOT NULL,
  `amazon_seller_id` varchar(70) NOT NULL,
  `marketplace_id` varchar(70) NOT NULL,
  `aws_access_key_id` varchar(100) NOT NULL,
  `secret_key` varchar(70) NOT NULL,
  `mws_auth_token` varchar(200) NOT NULL,
  `send_mail_option` varchar(50) NOT NULL,
  `watchdog_settings` varchar(70) NOT NULL,
  `bad_review_email` varchar(100) NOT NULL,
  `special_review_notifcation` varchar(20) NOT NULL,
  `account_username` varchar(50) NOT NULL,
  `account_password` varchar(50) NOT NULL,
  `email_smtp_host` varchar(50) NOT NULL,
  `email_smtp_username` varchar(50) NOT NULL,
  `email_smtp_password` varchar(50) NOT NULL,
  `email_smtp_from` varchar(100) NOT NULL,
  `amazon_aws_email_key` varchar(200) NOT NULL,
  `amazon_secret_email_key` varchar(200) NOT NULL,
  `amazon_verified_email` varchar(100) NOT NULL,
  `last_state_reset_time` varchar(100) NOT NULL,
  `database_host` varchar(100) NOT NULL,
  `database_username` varchar(100) NOT NULL,
  `database_password` varchar(100) NOT NULL,
  `aws_associated_tag` varchar(100) NOT NULL,
  `aws_access_key` varchar(100) NOT NULL,
  `aws_secret_key` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `az_settings`
--

INSERT INTO `az_settings` (`id`, `amazon_seller_id`, `marketplace_id`, `aws_access_key_id`, `secret_key`, `mws_auth_token`, `send_mail_option`, `watchdog_settings`, `bad_review_email`, `special_review_notifcation`, `account_username`, `account_password`, `email_smtp_host`, `email_smtp_username`, `email_smtp_password`, `email_smtp_from`, `amazon_aws_email_key`, `amazon_secret_email_key`, `amazon_verified_email`, `last_state_reset_time`, `database_host`, `database_username`, `database_password`, `aws_associated_tag`, `aws_access_key`, `aws_secret_key`) VALUES
(1, '', '', '', '', '', '', '', '', '', 'az_mm', 'az_@mm321', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `az_templates`
--

CREATE TABLE IF NOT EXISTS `az_templates` (
`id` int(11) NOT NULL,
  `template_name` varchar(100) NOT NULL,
  `template_subject` varchar(100) NOT NULL,
  `template_text` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `az_users`
--

CREATE TABLE IF NOT EXISTS `az_users` (
`id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('y','n') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `az_users`
--

INSERT INTO `az_users` (`id`, `username`, `email`, `password`, `last_updated`, `status`) VALUES
(1, 'az_mm', '', 'az_@mm321', '2015-01-27 05:53:28', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `az_watchdog_section`
--

CREATE TABLE IF NOT EXISTS `az_watchdog_section` (
`id` int(11) NOT NULL,
  `asin` varchar(50) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `az_autoresponders`
--
ALTER TABLE `az_autoresponders`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `az_customer_details`
--
ALTER TABLE `az_customer_details`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `az_email_statistics`
--
ALTER TABLE `az_email_statistics`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `az_products`
--
ALTER TABLE `az_products`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `az_reviews_details`
--
ALTER TABLE `az_reviews_details`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `az_reviews_status`
--
ALTER TABLE `az_reviews_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `az_settings`
--
ALTER TABLE `az_settings`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `az_templates`
--
ALTER TABLE `az_templates`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `az_users`
--
ALTER TABLE `az_users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `az_watchdog_section`
--
ALTER TABLE `az_watchdog_section`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `az_autoresponders`
--
ALTER TABLE `az_autoresponders`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `az_customer_details`
--
ALTER TABLE `az_customer_details`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `az_email_statistics`
--
ALTER TABLE `az_email_statistics`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `az_products`
--
ALTER TABLE `az_products`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `az_reviews_details`
--
ALTER TABLE `az_reviews_details`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `az_reviews_status`
--
ALTER TABLE `az_reviews_status`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `az_settings`
--
ALTER TABLE `az_settings`
MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `az_templates`
--
ALTER TABLE `az_templates`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `az_users`
--
ALTER TABLE `az_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `az_watchdog_section`
--
ALTER TABLE `az_watchdog_section`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
