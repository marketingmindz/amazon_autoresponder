<?php
ob_start();
include('db.php');
include('header.php');

$autoresponder_id = $_REQUEST['autoresponder_id'];
$query = mysql_query("select * from az_autoresponders where id='$autoresponder_id'");
$rows = mysql_fetch_assoc($query);


date_default_timezone_set("Asia/Calcutta");

if (isset($_POST['edit_autoresponder'])) {
    $template_name = $_POST['template_name'];
    $product_asin = $_POST['product_asin'];
    $cron_time = $_POST['cron_time'];
    $addTime = strtotime("+" . $cron_time . "hours");
    //$finalDate = date('y-m-d H', $addTime);
    $sql = "Update az_autoresponders SET template_name ='$template_name',product_asin='$product_asin',cron_time='$cron_time',running_time='$addTime' where id='$autoresponder_id'";

    if (mysql_query($sql)) {
        echo "<span style='color:green;font-weight:bold;text-align:center;'>Record updated successfully</span>";
        header("Location: autoresponder-section.php");
    } else
        echo "Error updating record: " . mysql_error();
}
?>

<div><h1 class="text_new_mm">Add New Autoresponder</h1>
    <form action="" method="post" name="autoresponderForm">
        <select id="select" name="template_name" style="width:90%; margin-bottom:12px;" required="required">
            <option value="">----Select Template----</option>
            <?php
            $sql = "Select * from az_autoresponders";
            $raws = mysql_query($sql);
            while ($results = mysql_fetch_array($raws)) {
                $selected = '';
                if ($rows['template_name'] == $results['template_name'])
                    $selected = " selected='selected'";
                echo '<option value=' . $results["template_name"] . '' . $selected . '>' . $results["template_name"] . '</option>';
            }
            ?>    
        </select>
        <select id="select" name="product_asin" style="width:90%; margin-bottom:12px;" required="required">
            <option value="">----Select ASIN----</option>
            <?php
            $AsinSql = "Select * from az_products";
            $raws = mysql_query($AsinSql);
            while ($results = mysql_fetch_array($raws)) {
                $selected = '';
                if ($rows['product_asin'] == $results['product_asin'])
                    $selected = " selected='selected'";
                echo '<option value=' . $results["product_asin"] . '' . $selected . '>' . $results["product_asin"] . '</option>';
            }
            ?>    
        </select>

        <select id="select" name="cron_time" style="width:90%; margin-bottom:12px;" required="required">
            <option value="">----Select Time----</option>
            <?php $selected = " selected='selected'"; ?>
            <option value="1" <?php if ($rows['cron_time'] == 1) echo $selected; ?>>1 Hour</option>
            <option value="12" <?php if ($rows['cron_time'] == 12) echo $selected; ?>>12 Hours</option>
            <option value="24" <?php if ($rows['cron_time'] == 24) echo $selected; ?>>24 Hours</option>
            <option value="48" <?php if ($rows['cron_time'] == 48) echo $selected; ?>>2 Days</option>
            <option value="72" <?php if ($rows['cron_time'] == 72) echo $selected; ?>>3 Days</option>
            <option value="120" <?php if ($rows['cron_time'] == 120) echo $selected; ?>>5 Days</option>
            <option value="144" <?php if ($rows['cron_time'] == 144) echo $selected; ?>>6 Days</option>
            <option value="168" <?php if ($rows['cron_time'] == 168) echo $selected; ?>>7 Days</option>
            <option value="336" <?php if ($rows['cron_time'] == 336) echo $selected; ?>>14 Days</option>
            <option value="720" <?php if ($rows['cron_time'] == 720) echo $selected; ?>>1 Month</option>
        </select>
        <input type="submit" name="edit_autoresponder" value="Update Autoresponder"/>
    </form></div>
<?php include('footer.php') ?>

