<?php
ob_start();
include('db.php'); 
session_start(); // Starting Session
?>
       <?php
        $actual_link = "$_SERVER[REQUEST_URI]";
        $page_title_exp = explode('/', $actual_link);
        $page_title = $page_title_exp[2];
        if ($page_title = str_replace('-', ' ', $page_title))
            $page_title = strtoupper($page_title);
        $page_title = explode('.', $page_title);
        $page_title = $page_title[0];
        ?>
        <?php 
		$title = explode('_',$page_title);
		?>
        <title><?php echo $title[0]." ".$title['1']; ?> </title>
        <link href="css/style.css" rel="stylesheet" type="text/css">
        <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="ckeditor/ckeditor.js"></script>

        <?php
        include('functions.php');
        include_once 'ckeditor/ckeditor.php';
      

        $ckeditor = new CKEditor();
        $ckeditor->basePath = '/ckeditor/';
        $ckeditor->config['filebrowserBrowseUrl'] = '/ckfinder/ckfinder.html';
        $ckeditor->config['filebrowserImageBrowseUrl'] = '/ckfinder/ckfinder.html?type=Images';
        $ckeditor->config['filebrowserFlashBrowseUrl'] = '/ckfinder/ckfinder.html?type=Flash';
        $ckeditor->config['filebrowserUploadUrl'] = '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
        $ckeditor->config['filebrowserImageUploadUrl'] = '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
        $ckeditor->config['filebrowserFlashUploadUrl'] = '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
        ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<body>

    <div id="profile">
        <div class="left__profile">
            <?php if (isset($_SESSION['login_user'])) { ?>
                <b id="welcome">Welcome : <?php echo strtoupper($_SESSION['login_user']) . "....!!"; ?></b>
			<?php } else { header("location: index.php"); 
				 die;
			}  ?>
            
           </div>
           <div class="right__profile"> <div class="i2Style__newbtn"><a href="logout.php">Log Out</a> </div></div>

        </div>

<div id="tabs">
            <ul>
             <li><a href="profile.php"><span>Home</span></a></li>
            <li><a href="settings.php"><span>Settings</span></a></li>
            <li><a href="product-section.php"><span>Product section</span></a></li>
            <li><a href="autoresponder-section.php"><span>Autoresponder section</span></a></li>
            <li><a href="template-section.php"><span>Template section</span></a></li>
           <!-- <li><a href="customers-section.php"><span>Customers section</span></a></li>-->
            <li><a href="email-statistics.php"><span>Email Statistics</span></a></li>
            <li><a href="watchdog-section.php"><span>Watchdog section</span></a></li>
        </ul>
    </div>
        
    
        <div class="cls"></div>   
    	<div id="middle__container">
